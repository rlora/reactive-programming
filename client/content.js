Template.content.helpers({
  "config": function() {
    return {
      fields: [{
        label: "One",
        name: "one",
        type: "text",
        validation: [{
          regex: /^[0-9]+$/,
          msg: "Only numbers allowed, no blanks nor spaces"
        }, {
          regex: /^[0-9]{5,10}$/,
          msg: "Must contain between 5 to 10 digits"
        }]
      }, {
        label: "Two",
        name: "two",
        type: "text"
      }, {
        label: "Three",
        name: "three",
        type: "text"
      }]
    };
  }
});