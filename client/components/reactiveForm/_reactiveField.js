Template._reactiveField.helpers({
  "fieldData": function() {
    return this.get();
  },
  "disabled": function() {
    if (this.disabled) {
      return {
        disabled: "disabled"
      };
    }
  },
  "readonly": function() {
    if (this.readonly) {
      return {
        readonly: "readonly"
      };
    }
  }
});

Template._reactiveField.events({
  "keypress, keyup": function() {
    var self = this;
    Meteor.setTimeout(function() {
      ReactiveForm.valideField(self.name);
    }, 50);
  }
});