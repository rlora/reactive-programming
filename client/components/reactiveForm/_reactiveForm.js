
ReactiveForm = Class(function() {
  var __fields = [];
  return {
    $singleton: true,
    getData: function() {
      return {
        "fields": __fields
      };
    },
    getField: function(name) {
      var target;
      target = _.find(__fields, function(f) {
        var field = f.get();
        return field.name === name;
      });
      return target;
    },
    getFieldValue: function(name) {
      return $("input[name=" + name + "]").val();
    },
    setField: function(name, field) {
      var target, targetData;
      target = this.getField(name);
      if (target) {
        targetData = target.get();
        _.extend(targetData, field);
        target.set(targetData);
      }
    },
    setFields: function(fields) {
      __fields = [];
      _.each(fields, function(f) {
        __fields.push(new ReactiveVar(f, function(a, b) {
          return false;
        }));
      });
    },
    getFields: function() {
      return _.map(__fields, function(f) {
        return f.get();
      });
    },
    valideField: function(name) {
      var target, targetData, targetValue;
      target = this.getField(name);
      if (target) {
        targetData = target.get();
        targetValue = this.getFieldValue(name);
        if (_.isArray(targetData.validation)) {
          //
          // Stop on first failure
          //
          _.find(targetData.validation, function(rule) {
            var isValid = rule.regex.test(targetValue);
            targetData.error = isValid ? "" : rule.msg;
            return (isValid === false);
          });
        }
        targetData.icon = (targetData.error === "")? "green check" : "";
        target.set(targetData);
      }
    }
  };
});

Template._reactiveForm.helpers({
  "formData": function() {
    var data;
    if (this.config) {
      ReactiveForm.setFields(this.config.fields);
      data = ReactiveForm.getData();
      return data;
    }
  }
});